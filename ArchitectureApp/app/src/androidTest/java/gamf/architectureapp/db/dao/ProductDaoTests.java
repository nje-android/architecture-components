package gamf.architectureapp.db.dao;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import gamf.architectureapp.db.AppDatabase;
import gamf.architectureapp.db.entities.Product;
import gamf.architectureapp.util.DataGenerator;
import gamf.architectureapp.util.QueryUtils;
import gamf.architectureapp.utils.LiveDataUtils;

@RunWith(AndroidJUnit4.class)
public class ProductDaoTests {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    private AppDatabase db;

    @Before
    public void init() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getInstrumentation().getContext(), AppDatabase.class)
                .allowMainThreadQueries()
                .build();
    }

    @After
    public void cleanup() {
        db.close();
    }

    @Test
    public void test_insert_successful() {
        Product p1 = DataGenerator.createProduct(1);
        db.productDao().insert(p1);

        int resultCount = db.productDao().count();
        Assert.assertEquals(1, resultCount);
    }

    @Test(expected = Exception.class)
    public void test_insert_duplicate() {
        Product p1 = DataGenerator.createProduct(1);
        Product p2 = DataGenerator.createProduct(1);
        db.productDao().insert(p1);
        db.productDao().insert(p2);
    }

    @Test
    public void test_query_product_list() throws InterruptedException {
        Product p1 = DataGenerator.createProduct(1);
        Product p11 = DataGenerator.createProduct(11);
        Product p2 = DataGenerator.createProduct(2);
        db.productDao().insert(p1);
        db.productDao().insert(p11);
        db.productDao().insert(p2);

        DataSource.Factory<Integer, Product> ds = db.productDao().getProductList(QueryUtils.handleLikeParam("1"));
        LiveData<PagedList<Product>> pagedListLiveData = new LivePagedListBuilder<Integer, Product>(ds, 50).build();
        PagedList<Product> products = LiveDataUtils.getValue(pagedListLiveData);

        Assert.assertEquals(2, products.size());

        Product result1 = products.get(0);
        Assert.assertNotNull(result1);
        Assert.assertEquals(p1.Id, result1.Id);

        Product result11 = products.get(1);
        Assert.assertNotNull(result11);
        Assert.assertEquals(p11.Id, result11.Id);
    }

    @Test
    public void test_get_product() throws InterruptedException {
        Product p1 = DataGenerator.createProduct(1);
        Product p2 = DataGenerator.createProduct(2);
        db.productDao().insert(p1);
        db.productDao().insert(p2);

        Product p = LiveDataUtils.getValue(db.productDao().getProductById(1));

        Assert.assertEquals(p1.Id, p.Id);
        Assert.assertEquals(p1.Name, p.Name);
        Assert.assertEquals(p1.Description, p.Description);
    }
}
