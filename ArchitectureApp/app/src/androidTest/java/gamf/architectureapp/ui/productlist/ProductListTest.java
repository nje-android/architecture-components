package gamf.architectureapp.ui.productlist;

import androidx.fragment.app.testing.FragmentScenario;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import gamf.architectureapp.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ProductListTest {

    @Test
    public void recreate_reload_query_text() {
        FragmentScenario scenario = FragmentScenario.launchInContainer(ProductListFragment.class);
        onView(withId(R.id.et_search)).perform(typeText("100"));
        scenario.recreate();

        onView(withId(R.id.et_search)).check(matches(withText("100")));
    }
}
