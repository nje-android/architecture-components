package gamf.architectureapp;

import android.app.Application;

import gamf.architectureapp.db.AppDatabase;

public class AcrhitectureApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //initialize singleton db instance with the application context
        AppDatabase.getInstance(getApplicationContext());
    }
}
