package gamf.architectureapp.util;

public interface ItemClickListener {
    void onClick(int id);
}
