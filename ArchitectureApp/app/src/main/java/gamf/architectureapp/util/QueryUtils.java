package gamf.architectureapp.util;

import android.text.TextUtils;

public final class QueryUtils {

    //Match all record if the param is null or empty
    public static String handleLikeParam(String param) {
        if (TextUtils.isEmpty(param)) return "%%";
        else return "%" + param + "%";
    }
}
