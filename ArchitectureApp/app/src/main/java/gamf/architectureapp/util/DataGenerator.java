package gamf.architectureapp.util;

import gamf.architectureapp.db.entities.Product;

public final class DataGenerator {

    public static Product createProduct(int index) {
        Product product = new Product();
        product.Id = index;
        product.Name = String.format("Product_%05d", index);
        product.Description = String.format("Description of %s", product.Name);
        return product;
    }
}
