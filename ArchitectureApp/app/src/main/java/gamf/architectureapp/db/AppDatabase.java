package gamf.architectureapp.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import gamf.architectureapp.db.dao.ProductDao;
import gamf.architectureapp.db.entities.Product;
import gamf.architectureapp.util.DataGenerator;

@Database(entities = {Product.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    synchronized public static AppDatabase getInstance(Context applicationContext) {

        if (instance == null) {
            instance = Room.databaseBuilder(applicationContext, AppDatabase.class, "app.db").build();
            instance.populateData();
        }

        return instance;
    }

    public abstract ProductDao productDao();


    private void populateData() {

        new Thread() {
            @Override
            public void run() {
                if (productDao().count() != 0) return;

                runInTransaction(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 1; i <= 500; i++) {
                            productDao().insert(DataGenerator.createProduct(i));
                        }
                    }
                });
            }
        }.start();

    }
}
