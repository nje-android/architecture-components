package gamf.architectureapp.db.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Product {
    @PrimaryKey
    public int Id;
    public String Name;
    public String Description;
}
