package gamf.architectureapp.db.dao;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import gamf.architectureapp.db.entities.Product;

@Dao
public abstract class ProductDao {

    @Insert
    public abstract void insert(Product product);

    @Query("SELECT COUNT(id) FROM Product")
    public abstract int count();

    @Query("SELECT * FROM Product WHERE name LIKE :query OR description LIKE :query ORDER BY name")
    public abstract DataSource.Factory<Integer, Product> getProductList(String query);

    @Query("SELECT * FROM Product WHERE id = :id")
    public abstract LiveData<Product> getProductById(int id);
}
