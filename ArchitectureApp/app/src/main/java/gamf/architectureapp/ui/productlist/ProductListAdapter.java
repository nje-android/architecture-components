package gamf.architectureapp.ui.productlist;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import gamf.architectureapp.R;
import gamf.architectureapp.db.entities.Product;
import gamf.architectureapp.util.ItemClickListener;

public class ProductListAdapter extends PagedListAdapter<Product, ProductListAdapter.ProductListViewHolder> {
    private static DiffUtil.ItemCallback<Product> DIFF_UTIL = new DiffUtil.ItemCallback<Product>() {
        @Override
        public boolean areItemsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem.Id == newItem.Id;
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem == newItem;
        }
    };
    private ItemClickListener mListener;

    public ProductListAdapter(ItemClickListener listener) {
        super(DIFF_UTIL);
        mListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        Product item = getItem(position);

        holder.nameTv.setText(item.Name);
        holder.descriptionTv.setText(item.Description);
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_list, parent, false);

        final ProductListViewHolder vh = new ProductListViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener == null) return;

                Product p = getItem(vh.getAdapterPosition());
                if (p == null) return;

                mListener.onClick(p.Id);
            }
        });

        return vh;
    }

    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTv;
        public TextView descriptionTv;

        public ProductListViewHolder(@NonNull View itemView) {
            super(itemView);

            nameTv = itemView.findViewById(R.id.label_name);
            descriptionTv = itemView.findViewById(R.id.content_name);
        }
    }
}
