package gamf.architectureapp.ui.productdetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import gamf.architectureapp.databinding.FragmentProductDetailBinding;

public class ProductDetailFragment extends Fragment {

    private FragmentProductDetailBinding mBinding;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final ProductDetailViewModel vm = ViewModelProviders.of(this).get(ProductDetailViewModel.class);
        final ProductDetailFragmentArgs args = ProductDetailFragmentArgs.fromBundle(getArguments());

        vm.setProductId(args.getProductId());
        mBinding.setVm(vm);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentProductDetailBinding.inflate(inflater, container, false);
        mBinding.setLifecycleOwner(this.getViewLifecycleOwner());
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }
}
