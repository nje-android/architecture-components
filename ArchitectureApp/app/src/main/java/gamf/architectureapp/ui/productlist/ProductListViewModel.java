package gamf.architectureapp.ui.productlist;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import gamf.architectureapp.db.AppDatabase;
import gamf.architectureapp.db.entities.Product;
import gamf.architectureapp.util.QueryUtils;

public class ProductListViewModel extends ViewModel {

    private AppDatabase db;
    private MutableLiveData<String> mSearchText = new MutableLiveData<>();
    public LiveData<PagedList<Product>> productList = Transformations.switchMap(mSearchText, new Function<String, LiveData<PagedList<Product>>>() {
        @Override
        public LiveData<PagedList<Product>> apply(String input) {

            PagedList.Config config = new PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPageSize(30)
                    .build();

            return new LivePagedListBuilder<Integer, Product>(
                    db.productDao().getProductList(QueryUtils.handleLikeParam(input)),
                    config
            ).build();
        }
    });

    {
        db = AppDatabase.getInstance(null);
        mSearchText.setValue("");
    }

    public LiveData<String> getSearchText() {
        return mSearchText;
    }

    public void setSearchText(String text) {
        if (mSearchText.getValue().equals(text))
            return;

        mSearchText.setValue(text);
    }
}
