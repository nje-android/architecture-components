package gamf.architectureapp.ui.productdetail;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import gamf.architectureapp.db.AppDatabase;
import gamf.architectureapp.db.entities.Product;

public class ProductDetailViewModel extends ViewModel {

    private AppDatabase db;
    private MutableLiveData<Integer> mProductId = new MutableLiveData<>();

    {
        db = AppDatabase.getInstance(null);
    }

    public LiveData<Product> product = Transformations.switchMap(mProductId, new Function<Integer, LiveData<Product>>() {
        @Override
        public LiveData<Product> apply(Integer id) {
            return db.productDao().getProductById(id);
        }
    });

    public void setProductId(Integer id) {
        Integer currentId = mProductId.getValue();

        if (currentId != null && currentId.equals(id)) return;
        mProductId.setValue(id);
    }
}
