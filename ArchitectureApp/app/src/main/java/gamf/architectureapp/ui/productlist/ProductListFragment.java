package gamf.architectureapp.ui.productlist;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import gamf.architectureapp.databinding.FragmentProductListBinding;
import gamf.architectureapp.db.entities.Product;
import gamf.architectureapp.util.CommonUtils;
import gamf.architectureapp.util.ItemClickListener;

public class ProductListFragment extends Fragment {

    private FragmentProductListBinding mBinding;

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {

        final ProductListViewModel vm = ViewModelProviders.of(this).get(ProductListViewModel.class);
        mBinding.setVm(vm);

        final EditText searchText = mBinding.etSearch;
        final RecyclerView rv = mBinding.rvProductList;

        final ProductListAdapter adapter = new ProductListAdapter(new ItemClickListener() {
            @Override
            public void onClick(int id) {
                ProductListFragmentDirections.ToProductDetailFragment direction = ProductListFragmentDirections.toProductDetailFragment(id);
                Navigation.findNavController(view).navigate(direction);
                CommonUtils.hideKeyboard(view);
            }
        });

        rv.setAdapter(adapter);
        rv.addItemDecoration(new DividerItemDecoration(rv.getContext(), ((LinearLayoutManager) rv.getLayoutManager()).getOrientation()));

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                vm.setSearchText(s.toString());
            }
        });

        vm.productList.observe(getViewLifecycleOwner(), new Observer<PagedList<Product>>() {
            @Override
            public void onChanged(PagedList<Product> products) {
                adapter.submitList(products);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentProductListBinding.inflate(inflater, container, false);
        mBinding.setLifecycleOwner(this.getViewLifecycleOwner());
        return mBinding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }
}
