package gamf.architectureapp.util;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

public class QueryUtilsTest {

    @Test
    public void test_like_param_empty() {
        String result1 = QueryUtils.handleLikeParam("");
        String result2 = QueryUtils.handleLikeParam(null);
        Assert.assertEquals("%%", result1);
        Assert.assertEquals("%%", result2);
    }

    @Test
    public void test_like_param_with_value() {
        String result1 = QueryUtils.handleLikeParam("test");
        Assert.assertEquals("%test%", result1);
    }
}